#!/usr/bin/env python3

from distutils.core import setup

setup(
    name='NaIfMenu',
    version='0.2',
    url='https://framagit.org/Daguhh/naifmenu',
    author = 'Daguhh',
    author_email = 'code.daguhh@zaclys.net',
    maintainer= 'Daguhh',
    maintainer_email = 'code.daguhh@zaclys.net',
    keywords = 'Menu PyQt5',
    license='The MIT License (MIT)',
)

