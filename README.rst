.. highlight:: none

NaïfMenu
========

    **naïf, naïve n. adj**.

    `1.` *LITTERAIRE* Qui est naturel, sans artifice, spontané. Art naïf, art populaire, folklorique. —  Un peintre naïf.

    `2.` *COURANT* Qui est plein de confiance et de simplicité par ignorance, par inexpérience. ➙ candide, ingénu, simple. —  Qui exprime des choses simples que tout le monde sait. Remarque naïve. ➙ simpliste.

    LeRobert_

.. contents:: Content
   :local:

----

        .. image:: https://framagit.org/Daguhh/naifmenu/-/raw/master/Screenshots/01_main_ui.png
          :width: 100px
          :align: center
          :alt: NaifMenu preview
  

NaïfMenu est un menu sans artifices simple à configurer pour organiser sur le vif vos scripts et applications. La configuration est réalisable entièrement par le simple déplacement de fichier .desktop et l'édition de fichier textes. En voici les fonctionnalités principales:

* Affichage et organisation en onglets
* Ajout de .desktop par glisser-déposer
* Mode réduit : le menu se réduit à une boîte flottante et reste accessible en un clic
*  Vue a deux panneaux (requiert i3wm)


Configuration
-------------

Les fichiers de configuration sont des fichiers textes et sont disponibles dans le dossier

::

    ~/.config/naifmenu/

Ajout d'un lanceur
^^^^^^^^^^^^^^^^^^

**méthode 1**


Un lanceur est un .desktop contenant à minima les entrées suivantes::

  [Desktop Entry]
  Name=My_App_Name
  Comment=My App Description
  Exec=My/app/path/my_app
  Icon=My/icon/path/my_icon.png

Et est à placer dans un dossier portant le nom de l'onglet::

    onglet/application.desktop

Ces dossiers, sont à placer eux même dans le dossier **Apps** du répertoire de configuration du menu, soit des chemins du type::

  ~/.config/naifmenu/Apps/onglet/application.desktop

**méthode 2**

Glissez-déposez un .desktop sur l'onglet ouvert pour l'y placer. Attention, si un dossier (correspondant à un onglet) est vide, il n'apparait pas dans le menu.

Paramètres du menu
^^^^^^^^^^^^^^^^^^

Les paramètres du menu sont sauvegardés et modifiables dans le fichier

.. code::

    ~/.config/naifmenu/config.ini


Sauvegarde du menu
^^^^^^^^^^^^^^^^^^

Les fichiers desktop sont rechargés uniquement si ils ont subit des modifications. Les lanceurs sont mis en cache dans le fichier::

    ~/.config/naifmenu/app_save.json

Ce fichier est automatiquement écrasé à chaque démarrage et ne doit pas être modifié.

Usage
-----

**méthode 1 (à préferer)**


.. code:: bash

    cd naifmenu
  ./naifmenu.py

**méthode 2 (expérimentale)**


Un paquet debian encore expérimental est disponible au test.
Si il ne présente pas, à priori, de risque pour votre distribution préférée, il est tout de même recommandé de l'installer dans une machine virtuelle dédiée.



.. _LeRobert: https://dictionnaire.lerobert.com/definition/naif


