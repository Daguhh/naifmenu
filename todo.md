### Todo List

* [x] correct max launcher per tab / add a limit?
* [x] crash on resize to one column
* [x] better parser for .desktop
* [x] if better parser => set language
* [ ] favorite tab?
* [ ] keyword search
* [ ] create a profile?
* [ ] dark/light mod
* [ ] remove button border
* [ ] resize text
* [x] resize icon with a slider / predifined values
* [x] do something about max launchers
* [ ] close app correctly when use window close button
* [ ] make reduce menu always on top

