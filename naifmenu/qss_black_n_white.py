#!/usr/bin/env python3

__author__ = "Toulibre"

"""
.. _start-doc-rst

=========================
Stylesheet specifications
=========================

Layout
======

Main windows
------------
::

    _________________________________________________
    |____|____|______________________________________|
    | ____                                       _   |
    | |   |--REDUCE_BTN_QSS   TWO_PANEL_CB_QSS--|_|  |
    | |___|                                      _   |----MAIN_WINDOW_QSS
    |                            REDUCE_CB_QSS--|_|  |
    |  _______________________                       |
    | |______|________|_______|_____________________ |
    | |  ________________________________________  | |
    | | |  ____________________________________  | |-+---TAB_BAR_QSS
    | | | | ______________                     | | | |
    | | | | |  __________ |                    | |-+-+---SCROLL_AREA_QSS
    | | | | | |         | |--APP_LAUNCHER_QSS  | | | |
    | | | | | |         | |                    |-+-+-+---TAB_QSS
    | | | | | |         |-+--APP_BUTTON_QSS    | | | |
    | | | | | |         | |                    | | | |
    | | | | | |_________| |                    | | | |
    | | | | |_____________|                    | | | |
    | | | |____________________________________| | | |
    | | |________________________________________| | |
    | |____________________________________________| |
    |________________________________________________|


Dialogs popup
-------------
::

    ICON_SIZE_DIALOG_QSS
    ICON_THEME_DIALOG_QSS
    PROGRESS_BAR_DIALOG_QSS


QtWidgets
=========

+--------------------------+-------------------------------------+
|    Style sheet           |             QtWidgets               |
+==========================+=====================================+
| MAIN_WINDOW_QSS          | - QMenuBar                          |
|                          | - QWidget                           |
+--------------------------+-------------------------------------+
| REDUCE_BTN_QSS           | - QPushButton                       |
+--------------------------+-------------------------------------+
| REDUCE_CB_QSS            | - QCheckBox                         |
+--------------------------+-------------------------------------+
| TWO_PANEL_CB_QSS         | - QCheckBox                         |
+--------------------------+-------------------------------------+
| TAB_BAR_QSS              | - QTabWidget                        |
|                          | - QWidget                           |
+--------------------------+-------------------------------------+
| SCROLL_AREA_QSS          | - QScrollArea                       |
+--------------------------+-------------------------------------+
| TAB_QSS                  | - QWidget                           |
+--------------------------+-------------------------------------+
| APP_LAUNCHER_QSS         | - QFrame                            |
+--------------------------+-------------------------------------+
| APP_BUTTON_QSS           | - QToolButton                       |
+--------------------------+-------------------------------------+
| ICON_SIZE_DIALOG_QSS     | - QDialog                           |
|                          | - QSlider                           |
|                          | - QLabel                            |
|                          | - QPushButton                       |
+--------------------------+-------------------------------------+
| ICON_THEME_DIALOG_QSS    | - QDialog                           |
|                          | - QFrame                            |
|                          | - QCheckBox                         |
|                          | - QPushButton                       |
+--------------------------+-------------------------------------+
| PROGRESS_BAR_DIALOG_QSS  | - QDialog                           |
|                          | - QProgressBar                      |
|                          | - QLabel                            |
+--------------------------+-------------------------------------+


.. _end-doc-rst:
"""

#####################################################
## Initialisation des variables à une valeur nulle ##
#####################################################

MAIN_WINDOW_QSS = ""
REDUCE_BTN_QSS = ""
REDUCE_CB_QSS = ""
TWO_PANEL_CB_QSS = ""
TAB_BAR_QSS = ""
SCROLL_AREA_QSS = ""
TAB_QSS = ""
APP_LAUNCHER_QSS = ""
APP_BUTTON_QSS = ""
ICON_SIZE_DIALOG_QSS = ""
ICON_THEME_DIALOG_QSS = ""
PROGRESS_BAR_DIALOG_QSS = ""

#####################################
############ Thèmes #################
#####################################




MAIN_WINDOW_QSS="""
    QMainWindow {
        background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #111, stop:1 #222);
    }
    QFrame {
        background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #222, stop:1 #111);
        font-size: normal;
        font-weight: normal;
    }
    QFrame:hover {
        background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #111, stop:1 #222);
    }
    QMenuBar {
        background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #fff, stop:1 #333);
        color: #444;
        font-weight: bold;
}
    QMenuBar::item {
        background: transparent;
}
    QMenuBar::hover {
        background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #333, stop:1 #fff);
}"""
