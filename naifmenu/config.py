#!/usr/bin/env python3

import os
import configparser
from itertools import chain
import glob


####### Config paths ######################
HOME = os.getenv("HOME")
CONFIG_PATH = f"{HOME}/.config/naifmenu"
APP_SAVE_FILE = f"{CONFIG_PATH}/app_save.json"
USER_CONFIG = f"{CONFIG_PATH}/config.ini"
DESKTOP_FILES_PATH = f"{CONFIG_PATH}/Apps"

####### Examples Files ################
APP_TAB_EXAMPLE = "/usr/share/naifmenu/example/tab"
APP_EXAMPLE = "/usr/share/naifmenu/example/app"

###### User define parameters #############
CONFIG = configparser.ConfigParser()
CONFIG.read(USER_CONFIG)

###### Icon paths #########################
ICON_PATHS = [f"{HOME}/.local/share/icons", "/usr/share/icons", "/usr/share/pixmaps"]
# ICON_SIZES = [96, 72, 64, 48, 36, 24, 16]
ICON_SIZES = (24, 32, 48, 64, 128)  # , 256)
# sizez = [(40,20),(80,40),(120,60),(160,80),(200,100),(240,120)]
ICON_DEFAULT = "/usr/share/naifmenu/example/app/empty.png"

###### App names #########################
MENU_NAME = "naifmenu"
MENU_TITLE = "NaïfMenu"

##### I3 related parameters ###############
I3_RIGHT = "right"
I3_LEFT = "left"

##### Default user parameters ############
DEFAULT_CONF_INI = """
[Icon]
x = 100
y = 60
theme = hicolor
size = 48
themes = hicolor

[Window]
x = 450
y = 350

[Options]
dualpanel = False
autoclose = True

[Extensions]
LayoutMgr = False
Header = True
"""


def make_configs():
    """Create config dir/files if they don't exist"""

    first_launch = False

    if not os.path.isdir(CONFIG_PATH):
        os.makedirs(CONFIG_PATH)

    if not os.path.isdir(f"{DESKTOP_FILES_PATH}/*"):
        if not os.path.isdir(f"{DESKTOP_FILES_PATH}"):
            os.makedirs(DESKTOP_FILES_PATH)
            os.system(f"cp -R {APP_TAB_EXAMPLE}/* {DESKTOP_FILES_PATH}/")
        # os.system(f'cp -R {cf.APP_EXAMPLE} {cf.CONFIG_PATH}')

    if not os.path.exists(USER_CONFIG):
        with open(USER_CONFIG, "w") as user_conf:
            user_conf.write(DEFAULT_CONF_INI)
        first_launch = True
    if not os.path.exists(APP_SAVE_FILE):
        pass

    return first_launch
