#!/usr/bin/env python3

"""
.. _start-doc-rst

=========================
Stylesheet specifications
=========================

Layout
======

Main windows
------------
::

    _________________________________________________
    |____|____|______________________________________|
    | ____                                       _   |
    | |   |--REDUCE_BTN_QSS   TWO_PANEL_CB_QSS--|_|  |
    | |___|                                      _   |----MAIN_WINDOW_QSS
    |                            REDUCE_CB_QSS--|_|  |
    |  _______________________                       |
    | |______|________|_______|_____________________ |
    | |  ________________________________________  | |
    | | |  ____________________________________  | |-+---TAB_BAR_QSS
    | | | | ______________                     | | | |
    | | | | |  __________ |                    | |-+-+---SCROLL_AREA_QSS
    | | | | | |         | |--APP_LAUNCHER_QSS  | | | |
    | | | | | |         | |                    |-+-+-+---TAB_QSS
    | | | | | |         |-+--APP_BUTTON_QSS    | | | |
    | | | | | |         | |                    | | | |
    | | | | | |_________| |                    | | | |
    | | | | |_____________|                    | | | |
    | | | |____________________________________| | | |
    | | |________________________________________| | |
    | |____________________________________________| |
    |________________________________________________|


Dialogs popup
-------------
::

    ICON_SIZE_DIALOG_QSS
    ICON_THEME_DIALOG_QSS
    PROGRESS_BAR_DIALOG_QSS


QtWidgets
=========

+--------------------------+-------------------------------------+
|    Style sheet           |             QtWidgets               |
+==========================+=====================================+
| MAIN_WINDOW_QSS          | - QMenuBar                          |
|                          | - QWidget                           |
+--------------------------+-------------------------------------+
| REDUCE_BTN_QSS           | - QPushButton                       |
+--------------------------+-------------------------------------+
| REDUCE_CB_QSS            | - QCheckBox                         |
+--------------------------+-------------------------------------+
| TWO_PANEL_CB_QSS         | - QCheckBox                         |
+--------------------------+-------------------------------------+
| TAB_BAR_QSS              | - QTabWidget                        |
|                          | - QWidget                           |
+--------------------------+-------------------------------------+
| SCROLL_AREA_QSS          | - QScrollArea                       |
+--------------------------+-------------------------------------+
| TAB_QSS                  | - QWidget                           |
+--------------------------+-------------------------------------+
| APP_LAUNCHER_QSS         | - QFrame                            |
+--------------------------+-------------------------------------+
| APP_BUTTON_QSS           | - QToolButton                       |
+--------------------------+-------------------------------------+
| ICON_SIZE_DIALOG_QSS     | - QDialog                           |
|                          | - QSlider                           |
|                          | - QLabel                            |
|                          | - QPushButton                       |
+--------------------------+-------------------------------------+
| ICON_THEME_DIALOG_QSS    | - QDialog                           |
|                          | - QFrame                            |
|                          | - QCheckBox                         |
|                          | - QPushButton                       |
+--------------------------+-------------------------------------+
| PROGRESS_BAR_DIALOG_QSS  | - QDialog                           |
|                          | - QProgressBar                      |
|                          | - QLabel                            |
+--------------------------+-------------------------------------+


.. _end-doc-rst:
"""

#####################################################
## Initialisation des variables à une valeur nulle ##
#####################################################

MAIN_WINDOW_QSS = ""
REDUCE_BTN_QSS = ""
REDUCE_CB_QSS = ""
TWO_PANEL_CB_QSS = ""
TAB_BAR_QSS = ""
SCROLL_AREA_QSS = ""
TAB_QSS = ""
APP_LAUNCHER_QSS = ""
APP_BUTTON_QSS = ""
ICON_SIZE_DIALOG_QSS = ""
ICON_THEME_DIALOG_QSS = ""
PROGRESS_BAR_DIALOG_QSS = ""

#####################################
############ Thèmes #################
#####################################

APP_LAUNCHER_QSS = """
    QFrame:hover {
        border: 2px solid green;
        border-radius: 4px;
        padding: 2px;
    }
    QFrame:pressed {
        border: 50px solid green;
        border-radius: 44px;
        padding: 2px;
    }
"""

APP_BUTTON_QSS = """
    QToolButton {
        border: None;
    }
    QPushButton:pressed {
        border: 50px solid white;
        border-radius: 60px;
        padding: 4px;
    }
"""

TAB_BAR_QSS = """
    QTabWidget::pane { /* The tab widget frame */
        border-top: 2px solid #0000F0;
        position: absolute;
        top: 0.5em;
    }

    QTabWidget::tab-bar {
        alignment: center;
    }

    /* Style the tab using the tab sub-control. Note that
        it reads QTabBar _not_ QTabWidget */
    QTabBar::tab {
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
        min-width: 8ex;
        padding-left: 10px;
        padding-right: 10px;
    }

    QTabBar::tab:selected, QTabBar::tab:hover {
        border-bottom: 3px solid #FF0000;
        border-right: 1px solid #DFDFDF;
        border-top: 1px solid #DFDFDF;
    }
    QWidget {
        border: None;
    }
"""
