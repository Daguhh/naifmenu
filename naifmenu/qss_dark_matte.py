#!/usr/bin/env python3

__author__ = "Toulibre"

"""
.. _start-doc-rst

=========================
Stylesheet specifications
=========================

Layout
======

Main windows
------------
::

    _________________________________________________
    |____|____|______________________________________|
    | ____                                       _   |
    | |   |--REDUCE_BTN_QSS   TWO_PANEL_CB_QSS--|_|  |
    | |___|                                      _   |----MAIN_WINDOW_QSS
    |                            REDUCE_CB_QSS--|_|  |
    |  _______________________                       |
    | |______|________|_______|_____________________ |
    | |  ________________________________________  | |
    | | |  ____________________________________  | |-+---TAB_BAR_QSS
    | | | | ______________                     | | | |
    | | | | |  __________ |                    | |-+-+---SCROLL_AREA_QSS
    | | | | | |         | |--APP_LAUNCHER_QSS  | | | |
    | | | | | |         | |                    |-+-+-+---TAB_QSS
    | | | | | |         |-+--APP_BUTTON_QSS    | | | |
    | | | | | |         | |                    | | | |
    | | | | | |_________| |                    | | | |
    | | | | |_____________|                    | | | |
    | | | |____________________________________| | | |
    | | |________________________________________| | |
    | |____________________________________________| |
    |________________________________________________|


Dialogs popup
-------------
::

    ICON_SIZE_DIALOG_QSS
    ICON_THEME_DIALOG_QSS
    PROGRESS_BAR_DIALOG_QSS


QtWidgets
=========

+--------------------------+-------------------------------------+
|    Style sheet           |             QtWidgets               |
+==========================+=====================================+
| MAIN_WINDOW_QSS          | - QMenuBar                          |
|                          | - QWidget                           |
+--------------------------+-------------------------------------+
| REDUCE_BTN_QSS           | - QPushButton                       |
+--------------------------+-------------------------------------+
| REDUCE_CB_QSS            | - QCheckBox                         |
+--------------------------+-------------------------------------+
| TWO_PANEL_CB_QSS         | - QCheckBox                         |
+--------------------------+-------------------------------------+
| TAB_BAR_QSS              | - QTabWidget                        |
|                          | - QWidget                           |
+--------------------------+-------------------------------------+
| SCROLL_AREA_QSS          | - QScrollArea                       |
+--------------------------+-------------------------------------+
| TAB_QSS                  | - QWidget                           |
+--------------------------+-------------------------------------+
| APP_LAUNCHER_QSS         | - QFrame                            |
+--------------------------+-------------------------------------+
| APP_BUTTON_QSS           | - QToolButton                       |
+--------------------------+-------------------------------------+
| ICON_SIZE_DIALOG_QSS     | - QDialog                           |
|                          | - QSlider                           |
|                          | - QLabel                            |
|                          | - QPushButton                       |
+--------------------------+-------------------------------------+
| ICON_THEME_DIALOG_QSS    | - QDialog                           |
|                          | - QFrame                            |
|                          | - QCheckBox                         |
|                          | - QPushButton                       |
+--------------------------+-------------------------------------+
| PROGRESS_BAR_DIALOG_QSS  | - QDialog                           |
|                          | - QProgressBar                      |
|                          | - QLabel                            |
+--------------------------+-------------------------------------+


.. _end-doc-rst:
"""

#####################################################
## Initialisation des variables à une valeur nulle ##
#####################################################

MAIN_WINDOW_QSS = ""
REDUCE_BTN_QSS = ""
REDUCE_CB_QSS = ""
TWO_PANEL_CB_QSS = ""
TAB_BAR_QSS = ""
SCROLL_AREA_QSS = ""
TAB_QSS = ""
APP_LAUNCHER_QSS = ""
APP_BUTTON_QSS = ""
ICON_SIZE_DIALOG_QSS = ""
ICON_THEME_DIALOG_QSS = ""
PROGRESS_BAR_DIALOG_QSS = ""

#####################################
############ Thèmes #################
#####################################


MAIN_WINDOW_QSS="""

QWidget {
   background-color: #222;
   color: white;
}
QWidget:hover {
   background-color: #222;
}
QPushButton:flat {
   background-color:  #333;
}
QPushButton:hover {
   background-color: #444;
}
QPushButton:pressed {
   background-color:  #333;

}
QMenuBar::item:selected {
    background-color: #333;
}
QMenu {
    background-color: #222;
}
QMenu:selected {
    background-color:  #333;
    border-radius: 5px;
    color: grey;
}
"""

TAB_BAR_QSS = """
QTabWidget {
    border: none;
}
QTabWidget::pane {
    border: none;
}
"""

SCROLL_AREA_QSS = """
QScrollArea {
    border: 1px solid #444;
}
QScrollArea:hover {
    border: 1px solid white;
}
"""

