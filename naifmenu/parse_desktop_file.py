#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
small desktop file parser
"""

from PyQt5.QtWidgets import QProgressDialog, QApplication
from PyQt5 import Qt
import os
import glob
import re
from itertools import product, chain
import hashlib
import json


from config import (
    ICON_PATHS,
    ICON_SIZES,
    CONFIG,
    CONFIG_PATH,
    APP_SAVE_FILE,
    ICON_DEFAULT,
    DESKTOP_FILES_PATH,
)

# from dialogs import AskWaitProgress


def get_app_for_folder():
    pass


def hash_file(file):
    """hash .desktop file to watch for any change"""

    hasher = hashlib.md5()
    with open(file, "rb") as f:
        buf = f.read()
        hasher.update(buf)
    return hasher.hexdigest()


def get_app_from_desktop():
    """
    get all app dict and store them in a list
    """

    # Try load previous session
    app_config_file = APP_SAVE_FILE
    try:
        with open(app_config_file) as json_data_file:
            saved_app = json.load(json_data_file)
    except FileNotFoundError as e:
        saved_app = {}

    app_list = []
    app_dct = {}

    # parse .desktop file if it's new
    tabs = os.listdir(DESKTOP_FILES_PATH)
    i, j = 0, 0
    progress_bar = QProgressDialog("Mise à jour des launchers", "Annuler", 0, 100)
    progress_bar.forceShow()
    # progress_bar.exec_()
    nb_files = len(glob.glob(f"{DESKTOP_FILES_PATH}/*/*"))
    stop_iteration = False
    for tab in tabs:

        path_desktop = f"{DESKTOP_FILES_PATH}/{tab}/*.desktop"
        for file in glob.iglob(path_desktop):

            hex_hash = hash_file(file)

            # if new launcher : parse .desktop
            if not hex_hash in list(saved_app.keys()):
                i += 1

                app = parse_desktop_lang(file)
                name = app["Name"]

                prompt = f"adding or updating\n{name}"
                temp = round(((i + j) / nb_files) * 100)
                progress_bar.setValue(temp)
                progress_bar.setLabelText(prompt)
                QApplication.processEvents()
                app["Icon"] = icon2paths(app["Icon"]).copy()
                app["Exec"] = txt2txt(app["Exec"])

                if progress_bar.wasCanceled():
                    stop_iteration = True
                    break

            # else load from json conf
            else:
                j += 1
                app = saved_app[hex_hash]

            category = file.split("/")[-2]
            app["category"] = category
            app_list += [app.copy()]
            app_dct[hex_hash] = app

        if stop_iteration:
            break

    progress_bar.setValue(100)
    print(
        f"\n{i} lauchers were added or updated,\nyou have now {i+j} launchers in your menu\n"
    )

    print("====== Starting menu =======")
    # Save session
    with open(app_config_file, "w") as outfile:
        json.dump(app_dct, outfile, indent=4)

    #  python function is not serializable so had to be done after loadin json
    for app in app_list:
        try:
            app["Exec"] = txt2fct(app["Exec"])
        except BadCommandDesktopError:
            app["Exec"] = ""

    return app_list


class BadCommandDesktopError(Exception):
    pass


def txt2txt(command_path):

    #    try:
    #        command = command_path.split('%')[0]
    #    except AttributeError:
    #        print("=============iBad comma,d====================")
    #        raise BadCommandDesktopError
    try:
        command = command_path.split("%")[0]
    except:
        command = command_path

    return command


def txt2fct(command_path):
    """ transform path of a command into a python function """
    command = txt2txt(command_path)

    def exec():
        os.system(command)

    return exec


def get_icons_themes():
    """Returns icon themes folders in cf.ICON_PATHS"""

    theme_list = {t.strip() for t in CONFIG["Icon"]["themes"].split(",")}
    theme_list.add("hicolor")
    return theme_list


def get_system_icons_themes():

    paths = ICON_PATHS

    themes = set(
        chain(
            *[
                [f.split("/")[-1] for f in glob.iglob(path + "/*") if os.path.isdir(f)]
                for path in paths
            ]
        )
    )

    themes.add("hicolor")

    theme_list = ", ".join(themes)
    print(
        f"""
Les thèmes d'icones suivants ont été trouvés sur le système:

{theme_list}

Pressez entrer pour charger touts les thèmes, ou entrez une nouvelle liste
Vous pourrez éditer cette liste dans .config/naifmenu/config.ini
"""
    )

    themes_ = input("Enter the inputs : ") or theme_list

    t1 = {t.strip() for t in theme_list.split(",")}
    t2 = {t.strip() for t in themes_.split(",")}
    wrong_entries = t2.difference(t1)
    good_entries = t2.intersection(t1)

    print(f"Entrées valides : {good_entries}")
    print(f"Entrées non valides : {wrong_entries}")
    themes_ = input("voulez vous changer? Entrez une nouvelle liste") or ", ".join(
        good_entries
    )

    CONFIG["Icon"]["themes"] = themes_
    # return themes_


def icon2paths(icon_name):
    """
    return icon path from icon_name :
        - if icon_name is path to icon, do nothing
        - if not look for icon in file system
    """
    themes = get_icons_themes()
    icon_path = {
        theme: {str(size): ICON_DEFAULT for size in ICON_SIZES} for theme in themes
    }

    i = 0
    if not icon_name:
        pass
    elif os.path.isfile(icon_name):
        for theme in themes:  # COaNFIG['Themes']:
            for size in ICON_SIZES:
                icon_path[theme][size] = icon_name
    else:
        for theme in themes:  # CaaONFIG['Themes']:
            t = theme
            for S in ICON_SIZES:
                s = str(S)
                hicolor_tmp = []
                for p in ICON_PATHS:
                    hicolor_tmp = glob.glob(
                            f"{p}/{icon_name}.*"
                    )
                    if hicolor_tmp:
                        break
                    hicolor_tmp = glob.glob(
                        f"{p}/hicolor/{s}x{s}/**/{icon_name}.*", recursive=True
                    )
                    if hicolor_tmp:
                        break

                for p in ICON_PATHS:
                    i += 1
                    icon_tmp = glob.glob(
                        f"{p}/{t}/{s}x{s}/**/{icon_name}.*", recursive=True
                    )
                    if icon_tmp:
                        icon_path[t][s] = icon_tmp[0]
                        break
                    icon_tmp = glob.glob(
                        f"{p}/{t}/**/{s}/{icon_name}.*", recursive=True
                    )
                    if icon_tmp:
                        icon_path[t][s] = icon_tmp[0]
                        break
                if icon_path[t][s] == ICON_DEFAULT:
                    if hicolor_tmp:
                        icon_path[t][s] = hicolor_tmp[0]

    return icon_path


def create_pattern(entry, lang=None):
    """Return a pattern to parse either or not translated entry in .desktop"""

    if lang != None:
        pattern = re.compile(f"^{entry}\[{lang}\]=.*")
    else:
        pattern = re.compile(f"^{entry}=.*")
    return pattern


def find_in_file(file, pattern):
    """return a matched pattern in a file"""

    match = None
    with open(file) as f:
        for line in f:
            temp = pattern.match(line)
            if temp != None:
                match = temp.string.replace("\n", "").split("=")[-1]
                break
    return match


def get_dropped_desktop(file_name, lang="fr"):

    app = parse_desktop_lang(file_name, lang)
    app["Icon"] = icon2paths(app["Icon"]).copy()
    app["Exec"] = txt2fct(app["Exec"])

    return app


def parse_desktop_lang(file_name, lang="fr"):
    """
    parse_desktop("fichier.desktop") => dict

    args:
        str : desktop file following freedesktop guidelines
    return:
        dict : parsed desktop file into dict
    """

    entry_names_lang = ["Name", "Comment"]
    entry_names = ["Exec", "Icon"]

    app = {}
    for name in entry_names_lang:
        pattern = create_pattern(name, lang)
        match = find_in_file(file_name, pattern)

        if match == None:
            pattern = create_pattern(name)
            match = find_in_file(file_name, pattern)

        app[name] = match

    for name in entry_names:
        pattern = create_pattern(name)
        match = find_in_file(file_name, pattern)

        app[name] = match

    return app


def parse_desktop(file_name):
    """
    parse_desktop("fichier.desktop") => dict

    args:
        str : desktop file following freedesktop guidelines
    return:
        dict : parsed desktop file into dict
    """

    entries = ["Name", "Comment", "Exec", "Icon"]

    Name = re.match(f"Name\[{lang}\]")
    app = {}
    with open(file_name, "r") as file:
        next(file)  # forget first line
        for line in file:
            k, v = [l.strip() for l in line.split("=")]
            app[k] = v

    app["Exec"] = txt2fct(app["Exec"])

    return app


if __name__ == "__main__":

    app = parse_desktop("qrcode.desktop")
    for k, v in app.items():
        pass
    app_list = get_app_from_desktop()
