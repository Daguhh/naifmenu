#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""
A simple menu to display .desktop files.
Powered by PyQt5.

@Author = Daguhh
"""

# Standard
import sys
import os
import shutil
import glob
from itertools import product, count
from functools import reduce
import subprocess
import time
import random
from itertools import chain

# import sip

# Installed
from PyQt5.QtCore import QSize
from PyQt5.QtCore import Qt, QByteArray
from PyQt5.QtGui import QPixmap, QIcon, QFont, QImage  # QStaticText, QColor,
from PyQt5.QtCore import QThreadPool, QRunnable, pyqtSlot
from PyQt5.QtWidgets import (
    QDialog,
    QMainWindow,
    QScrollArea,
    QWidget,
    QTabWidget,
    QHBoxLayout,
    QVBoxLayout,
    QAction,
    QLabel,
    QApplication,
    QPushButton,
    QGridLayout,
    QSizePolicy,
    QGraphicsOpacityEffect,
    QFrame,
    QCheckBox,
    QMessageBox,
    QAbstractButton,
    QToolButton,
)

# Local
from parse_desktop_file import (
    get_app_from_desktop,
    get_dropped_desktop,
    get_icons_themes,
    get_system_icons_themes,
)
import qss as qss
from layout_manager.layout_manager_tab import LayoutMgr
from dialogs import AskMultipleValues, AskIconSize, AskSystemIconThemes
import config as cf
import icons as icon

# Create conf files if do not exist
global g_icon_theme
global g_icon_size


class MainWindow(QMainWindow):

    instance = None

    def __init__(self):

        super().__init__()

        global g_icon_theme
        global g_icon_size

        is_first_launch = cf.make_configs()
        if is_first_launch:
            restart_program()

        g_icon_theme = cf.CONFIG["Icon"]["theme"]
        g_icon_size = int(cf.CONFIG["Icon"]["size"])

        self.title = cf.MENU_TITLE
        self.setWindowTitle(self.title)
        MainWindow.instance = self

        self.setStyleSheet(qss.MAIN_WINDOW_QSS)

        # Load conf files
        self.load_config()

        self.initUI()

        # self.showFullScreen()
        #self.setWindowFlag(Qt.WindowMinimizeButtonHint, True)
        #self.setWindowFlag(Qt.WindowMaximizeButtonHint, True)
        #self.setWindowFlags(Qt.FramelessWindowHint)
        #self.setWindowFlags(Qt.CustomizeWindowHint)
        #self.setWindowFlags(Qt.Widget)

        self.save_config()
        self.show()

    def initUI(self):
        """Create main window"""

        #### Status bar ####
        self.statusBar()

        #### Menubar ####
        # Close app
        exitAct = QAction(text="&Exit", parent=self)
        exitAct.setShortcut("Ctrl+Q")
        exitAct.setStatusTip("Exit application")
        exitAct.triggered.connect(self.quit)
        # exitAct.triggered.connect(QApplication.quit)

        #        saveAct = QAction(text="Save config", parent=self)
        #        saveAct.setStatusTip("Save menu configuration")
        #        saveAct.triggered.connect(self.save_config)

        delnreloadAct = QAction(text="Reset", parent=self)
        delnreloadAct.setShortcut("Ctrl+R")
        delnreloadAct.setStatusTip("Delete configurations files and reload the menu")
        delnreloadAct.triggered.connect(self.del_n_reload)

        #  Resize icons
        resizeiconAct = QAction(text="&Resize", parent=self)
        resizeiconAct.triggered.connect(self.get_size_value_popup)

        sysiconthemeAct = QAction(text="&Get themes", parent=self)
        sysiconthemeAct.setStatusTip("Choisissez quels thèmes du système charger")
        sysiconthemeAct.triggered.connect(self.get_system_icons_themes)

        #  Create menubar
        menubar = self.menuBar()
        fileMenu = menubar.addMenu("&Fichier")
        fileMenu.addAction(exitAct)
        # fileMenu.addAction(saveAct)
        fileMenu.addAction(delnreloadAct)
        EditMenu = menubar.addMenu("&Edition")
        IconMenu = menubar.addMenu("&Icons")
        IconMenu.addAction(resizeiconAct)
        IconMenu.addAction(sysiconthemeAct)
        themeMenu = IconMenu.addMenu("Set theme")

        # Change icon theme
        # print(get_icons_themes())
        for theme in get_icons_themes():  # cf.CONFIG["Themes"]:
            iconAct = QAction(text=theme, parent=self)
            iconAct.triggered.connect(self.set_icon_theme)
            themeMenu.addAction(iconAct)

        self.toggleLayoutmgrAct = QAction(text="LayoutMgr", checkable=True, parent=self)
        self.toggleLayoutmgrAct.setStatusTip("Cacher l'onglet layout")
        self.toggleLayoutmgrAct.triggered.connect(self.toggle_layout_mgr)
        self.toggleLayoutmgrAct.setChecked(
            cf.CONFIG["Extensions"].getboolean("LayoutMgr")
        )
        EditMenu.addAction(self.toggleLayoutmgrAct)

        self.toggleHeaderAct = QAction(text="Header", checkable=True, parent=self)
        self.toggleHeaderAct.setStatusTip("Hide Menu header")
        self.toggleHeaderAct.triggered.connect(self.toggle_header)
        self.toggleHeaderAct.setChecked(
            cf.CONFIG["Extensions"].getboolean("Header")
        )
        EditMenu.addAction(self.toggleHeaderAct)
        #### control panel ####
        window = QWidget()
        self.setCentralWidget(window)

        # Reduce mainwindow pushbutton
        reduceBtn = QPushButton()
        reduceBtn.setIcon(iconFromBase64(icon.MENU))
        reduceBtn.setIconSize(QSize(35, 35))
        reduceBtn.setFixedSize(50, 50)
        reduceBtn.setToolTip("Réduire le menu")
        reduceBtn.setFont(QFont("Times", 32))
        reduceBtn.clicked.connect(MainWindow.reduce_mainwindow)
        reduceBtn.setStyleSheet(qss.REDUCE_BTN_QSS)

        # Toggle two panel view
        self.twopanelCb = QCheckBox()  # "Vue à deux panneaux")
        self.twopanelCb.setToolTip(
            "Disposer les fenêtres en deux panneaux séparées verticalement"
        )
        self.twopanelCb.setIcon(iconFromBase64(icon.DUAL_PANEL))
        # cf.CONFIG["Options"].getboolean("autoclose")
        self.twopanelCb.setChecked(cf.CONFIG["Options"].getboolean("dualpanel"))
        self.twopanelCb.setStyleSheet(qss.TWO_PANEL_CB_QSS)

        # Toggle reduce mode
        self.reduceCb = QCheckBox()  # "Réduire le menu")
        self.reduceCb.setIcon(QIcon(iconFromBase64(icon.REDUCE)))
        self.reduceCb.setToolTip("Réduire le menu au lancement d'une application")
        self.reduceCb.setChecked(cf.CONFIG["Options"].getboolean("autoclose"))
        self.reduceCb.setStyleSheet(qss.REDUCE_CB_QSS)

        #### Tabs  ####
        # Create app launcher tabs
        self.table_widget = TabsContainer(parent=self)

        # Add Modules
        if self.toggleLayoutmgrAct.isChecked():
            self.layout_mgr = LayoutMgr()
            self.table_widget.addtabmodule(self.layout_mgr, "layout")
            self.twopanelCb.stateChanged.connect(self.layout_mgr.refresh)
            self.enable_modules()
        else:
            self.twopanelCb.setDisabled(True)

        # Window
        vbox = QVBoxLayout()
        window.setLayout(vbox)

        if self.toggleHeaderAct.isChecked():

            hbox = QHBoxLayout()
            vbox.addLayout(hbox)

            hbox.addWidget(reduceBtn)
            hbox.addStretch(1)
            vbox2 = QVBoxLayout()
            vbox2.addWidget(self.twopanelCb)
            vbox2.addWidget(self.reduceCb)
            hbox.addLayout(vbox2)

        vbox.addWidget(self.table_widget)

        self.show()

    def quit(self):

        self.save_config()
        QApplication.quit()

    def get_system_icons_themes(self):

        global g_icon_theme

        old_themes = {t.strip() for t in cf.CONFIG["Icon"]["themes"].split(",")}

        ask_theme = AskSystemIconThemes(old_themes)

        if not ask_theme.exec_():
            return

        themes = ask_theme.get_activated()
        themes.add("hicolor")

        cf.CONFIG["Icon"]["themes"] = ", ".join(themes)
        g_icon_theme = "hicolor"
        self.save_config()
        clear_cache()
        restart_program()

        # get_system_icons_themes()

    def toggle_layout_mgr(self):
        state = self.toggleLayoutmgrAct.isChecked()
        cf.CONFIG["Extensions"]["LayoutMgr"] = str(state)
        self.save_config()
        restart_program()
        pass

    def toggle_header(self):
        state = self.toggleHeaderAct.isChecked()
        cf.CONFIG["Extensions"]["Header"] = str(state)
        self.save_config()
        restart_program()

    def load_config(self):
        """Load user-defined MainWindow paramerters"""

        x = int(cf.CONFIG["Window"]["x"])
        y = int(cf.CONFIG["Window"]["y"])
        self.resize(x, y)

    def save_config(self):
        """Save user preferences in a text file"""
        global g_icon_size
        global g_icon_theme

        cf.CONFIG["Icon"]["theme"] = g_icon_theme
        cf.CONFIG["Icon"]["size"] = str(g_icon_size)
        cf.CONFIG["Options"]["autoclose"] = str(self.reduceCb.isChecked())
        cf.CONFIG["Options"]["dualpanel"] = str(self.twopanelCb.isChecked())
        cf.CONFIG["Extensions"]["LayoutMgr"] = str(self.toggleLayoutmgrAct.isChecked())

        with open(cf.USER_CONFIG, "w") as configfile:
            cf.CONFIG.write(configfile)

    def del_config(self):
        """remove config files"""

        os.system(f"rm -Rf {cf.USER_CONFIG}")

    def del_n_reload(self):

        self.del_config()
        restart_program()

    def enable_modules(self):
        """Enable optional modules"""

        # check if i3wm is running
        # cmd = r'wmctrl -m | sed -nr "s/Name: (.*)/\1/p"'
        # wm_name = subprocess.check_output(cmd, shell=True, text=True).rstrip()
        if shutil.which("i3") == None:
            self.twopanelCb.setDisabled(True)
            self.twopanelCb.setToolTip(
                "Dual Panel module : please install and run i3wm"
            )
            self.layout_mgr.setToolTip(
                "Dual Panel module : please install and run i3wm"
            )

    @classmethod
    def reduce_mainwindow(cls):
        """Toggle reduce mode and big picture from button activation"""

        # if cls.instance.reduceCb.isChecked() == False:
        #    return
        dialog = ReduceModButton()
        MainWindow.instance.hide()
        if dialog.exec_():
            vals = dialog.accept()
            cls.instance.show()
        else:
            QApplication.quit()
        return

    def get_size_value_popup(self, *args):
        """Get new icon size from dialog"""
        global g_icon_size

        self.dialog_s = AskIconSize(g_icon_size)
        self.dialog_s.slider.valueChanged.connect(self.act_resize_icon)
        self.dialog_s.exec_()

    def act_resize_icon(self):
        global g_icon_theme
        global g_icon_size

        val = self.dialog_s.get_new_size()
        g_icon_size = cf.ICON_SIZES[val]

        # x = 2 * size
        # y = size

        # cf.CONFIG["Icon"]["x"] = str(x)
        # cf.CONFIG["Icon"]["y"] = str(y)
        # cf.CONFIG['Icon']['size'] = str(size)
        # theme = cf.CONFIG['Icon']['theme']

        self.update_launchers()

        TabsContainer.instance.resizeEvent(None)
        # self.refresh()

        # for launcher in AppLauncherBtn.instances.values():
        #    launcher.set_icon_theme_2(g_icon_theme)
        #    #launcher.resize_icons_2(*size)
        # for tab in Tab.instances.values():
        #    tab.launcher_size = size #(x, y)

    def set_icon_theme(self):
        """Change all icon theme"""

        global g_icon_theme

        g_icon_theme = self.sender().text()
        # cf.CONFIG["Icon"]["theme"] = theme

        self.update_launchers()

        # for launcher in AppLauncherBtn.instances.values():
        #    launcher.set_icon_theme_2(theme)

    def update_launchers(self):

        for launcher in AppLauncherBtn.instances.values():
            launcher.update()

    def resizeEvent(self, event):
        """Capture new window size for saving purpose"""

        x, y = self.size().width(), self.size().height()
        cf.CONFIG["Window"]["x"] = str(x)
        cf.CONFIG["Window"]["y"] = str(y)


class ReduceModButton(QDialog):
    """ button that reopen main window """

    def __init__(self):

        super(QDialog, self).__init__()
        self.layout = QGridLayout(self)  # QVBoxLayout(self)

        # Button properties
        reopen_button = QPushButton()
        reopen_button.setIcon(iconFromBase64(icon.MENU))
        reopen_button.setIconSize(QSize(64, 64))
        reopen_button.setGeometry(0, 0, 80, 80)
        reopen_button.setStatusTip("Afficher le menu en plein écran")
        reopen_button.clicked.connect(self.accept)
        reopen_button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        reopen_button.setStyleSheet("background-color:rgba(55,55,55,15);")
        reopen_button.setFont(QFont("Times", 48))
        self.setGeometry(0, 0, 90, 90)

        # Position on screen
        dialog_size = self.geometry().getRect()
        desktop_size = QApplication.desktop().screenGeometry().getRect()
        *_, x, y = map(lambda x, y: y - x - 30, dialog_size, desktop_size)
        self.move(x, y)

        # Add trnasparency
        self.layout.addWidget(reopen_button)
        op = QGraphicsOpacityEffect(self)
        op.setOpacity(0.5)  # 0 to 1 will cause the fade effect to kick in
        self.setGraphicsEffect(op)
        self.setAutoFillBackground(True)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)


class TabsContainer(QWidget):
    """
    A container (Qwidget) to hold all category tabs
    at call it loops over desktop files, create new category tab,
    and fill them with launcher for apps
    """

    instance = None

    def __init__(self, parent):
        super(QWidget, self).__init__(parent)

        TabsContainer.instance = self
        self.layout = QVBoxLayout(self)

        # Initialize tab screen
        self.tabs = QTabWidget()
        x = int(cf.CONFIG["Window"]["x"])
        y = int(cf.CONFIG["Window"]["y"])
        # self.tabs.resize(x, y)

        self.tabs.setStyleSheet(qss.TAB_BAR_QSS)
        # get all apps
        app_list = get_app_from_desktop()

        # Create tabs and give them a name
        categories = set([app["category"] for app in app_list])
        for category in categories:
            tab = ScrollTab(category)
            name = category
            self.tabs.addTab(tab, name)

        # Fill tabs with apps with launchers
        for app in app_list:
            category = app["category"]
            Tab.instances[category].addLauncher(app)

        # Add tabs to widget
        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)

    def addtabmodule(self, module, tab_name):

        self.tabs.addTab(module, tab_name)

    def resizeEvent(self, e):

        for tab in Tab.instances.values():

            tab.resizeEvent2(self.size().width())


class ScrollTab(QScrollArea):
    """
    Make tab vertically scrollable
    """

    def __init__(self, category):

        super(QScrollArea, self).__init__()
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setWidgetResizable(True)

        self.setStyleSheet(qss.SCROLL_AREA_QSS)

        self.widget = Tab(category)
        self.setWidget(self.widget)


class Tab(QWidget):
    """
    A tab for each category,
    support .desktop files dropEvent
    laucnhers are display on a grid that reshapes on window resizeEvent
    """

    instances = {}

    def __init__(self, category):
        """ Create an empty tab """

        global g_icon_size
        global g_icon_theme

        super(QWidget, self).__init__()

        self.setAcceptDrops(True)

        # max launcher in 1 Tab
        self.setStyleSheet(qss.TAB_QSS)

        # keep track of all created categories
        self.category = category
        Tab.instances[category] = self

        # keep track of launchers
        self.launcher_list = []

        # Init grid, its shape and a generator to fill it
        self.layout = QGridLayout(self)
        self.layout.setSpacing(0)

        # self.shape = (3,5) # 3x3 squares
        self.width = 3
        self.max_launcher = 1000  # useless?
        self.gen_position = self.genPos()

        # x = int(cf.CONFIG["Icon"]["x"])
        # y = int(cf.CONFIG["Icon"]["y"])
        # size = int(cf.CONFIG['Icon']['size'])
        # self.launcher_size = size #(x, y)

        # self.setMinimumWidth((self.launcher_size[0]))

    def addLauncher(self, app):
        """ create a button to launch app, store it as "button" key"""

        app["button"] = AppLauncherBtn(self.layout, app, next(self.gen_position))
        self.launcher_list += [app]

    def genPos(self):
        """
        Create a generator that return positions,
        line by line, to fill a 2-dimensions grid
        args:
            shape(tuple) : (x,y) dimensions of the array to fill
        return:
            generator
        """

        i = 0
        while i <= self.max_launcher:  # shape[0] * shape[1]:
            x = i // self.width
            y = i % self.width
            yield (x, y)
            i += 1

    @property
    def launcher_size(self):
        global g_icon_size
        return g_icon_size * 2 + 20, g_icon_size + 35

    def resizeEvent2(self, tab_x):
        """ Modify grid shape as window is resized """
        global g_icon_size

        # tab_x = self.scroll_area.tab_container.size().width()

        C1 = 40  # tab_in border = tab_out border - 20
        C2 = 20  # icon_size + 20
        eps = 5  # to make launcher size a little bigger
        new_width = (tab_x - C1) // (self.launcher_size[0])
        self.width = new_width if new_width > 0 else 1
        self.refresh()

    def refresh(self):

        # print(""" refresh """)

        self.gen_position = self.genPos()

        for app in self.launcher_list:
            self.layout.removeItem(app["button"].layout)
        for app in self.launcher_list:
            x, y = next(self.gen_position)
            self.layout.addWidget(app["button"], x, y, Qt.AlignCenter)

    def dragEnterEvent(self, e):
        """ A useless method """

        if e.mimeData().hasFormat("text/uri-list"):
            e.accept()
        else:
            e.ignore()

    def dropEvent(self, e):
        """ As desktop file is dropped on a tab, create a new launcher """

        file_path = e.mimeData().urls()[0].path()  # split('//')[1:][0]
        app = get_dropped_desktop(file_path)
        file_name = file_path.split("/")[-1]
        os.system(f"cp {file_path} {cf.DESKTOP_FILES_PATH}/{self.category}/{file_name}")
        self.addLauncher(app)


# class AppLauncherBtn(QAbstractButton):


class AppLauncherBtn(QFrame):
    instances = {}

    def __init__(self, parent_tab, app, pos):

        global g_icon_size
        global g_icon_theme

        super(QFrame, self).__init__()

        self.setFixedSize(*self.launcher_size)
        self.setStyleSheet(qss.APP_LAUNCHER_QSS)
        self.layout = QVBoxLayout(self)

        name, self.icons, tooltip = app["Name"], app["Icon"], app["Comment"]
        icon = self.icons[g_icon_theme][str(g_icon_size)]

        self.btn = QToolButton()
        self.btn.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.btn.setIcon(QIcon(QPixmap(icon)))
        self.btn.setIconSize(QSize(g_icon_size, g_icon_size))
        # if len(name) > 10:
        #    name = name[:10] + name[10:].replace(' ','\n',1)
        name = self.resize_string_(name)
        # name = name[:10] + '\n' + name[10:]
        self.btn.setText(name)
        self.btn.setFont(QFont("System", 8))
        # self.btn.setStyleSheet(qss.APP_BUTTON_QSS.replace('(color)', str(c)))
        self.btn.setStyleSheet(qss.APP_BUTTON_QSS)
        #self.btn.setToolTip(tooltip)
        self.btn.setStatusTip(tooltip)
        self.btn.clicked.connect(self.run_app)

        self.btn.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        # self.btn.setAlignment(Qt.AlignCenter)
        self.layout.addWidget(self.btn, Qt.AlignCenter)
        self.setLayout(self.layout)

        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)

        self.threadpool = QThreadPool()
        self.app_command = app["Exec"]

        parent_tab.addWidget(self, *pos)

        AppLauncherBtn.instances[name] = self

    def resize_string_(self, sentence, sep=" ", length=15):
        """Format one line long sentence into multilines
        Format a sentence given a maximum line size without
        cutting words.

        Parameters
        ----------
        sep : str
            word separator
        length = maximum line size

        Returns
        -------
        str
            Multiline converted sentence

        Examples
        --------

        >>> one_line_text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        >>> multi_line_text = resize_string_(one_line_text)
        >>> print(multi_line_text)
        Lorem ipsum dolor sit amet,
        consectetur adipiscing elit,
        sed do eiusmod tempor
        incididunt ut labore et dolore
        magna aliqua.

        """

        return reduce(
            lambda x, p: x + "\n" + p
            if len(x.rsplit("\n")[-1]) + len(p) > length
            else x + sep + p,
            sentence.split(sep),
        )

    def run_app(self):

        print("--- run ---")

        app_runner = AppRunner(self.app_command)
        self.threadpool.start(app_runner)
        if MainWindow.instance.reduceCb.isChecked():
            MainWindow.reduce_mainwindow()

    @property
    def launcher_size(self):
        global g_icon_size
        return g_icon_size * 2 + 20, g_icon_size + 35

    def update(self):
        global g_icon_size
        global g_icon_theme

        icon = self.icons[g_icon_theme][str(g_icon_size)]
        self.setFixedSize(*self.launcher_size)
        self.btn.setIcon(QIcon(QPixmap(icon)))
        self.btn.setIconSize(QSize(g_icon_size, g_icon_size))


class AppRunner(QRunnable):
    """ Run app in a thread """

    def __init__(self, app_command):

        super(AppRunner, self).__init__()
        self.command = app_command

    @pyqtSlot()
    def run(self):
        self.command()


# def get_icons_themes_old():
#    """Returns icon themes folders in cf.ICON_PATHS"""
#
#    paths = cf.ICON_PATHS
#
#    themes = set(
#        chain(
#            *[
#                [f.split("/")[-1] for f in glob.iglob(path + "/*") if os.path.isdir(f)]
#                for path in paths
#            ]
#        )
#    )
#
#    return themes


def iconFromBase64(base64):
    """Read menu icon images from base64 string"""

    pixmap = QPixmap()
    pixmap.loadFromData(QByteArray.fromBase64(base64))
    icon = QIcon(pixmap)
    return icon


def clear_cache():
    if os.path.exists(cf.APP_SAVE_FILE):
        os.remove(cf.APP_SAVE_FILE)


def restart_program():
    """Reload menu"""

    python = sys.executable
    os.execl(python, python, *sys.argv)


if __name__ == "__main__":

    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())
