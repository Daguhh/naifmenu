#!/usr/bin/env python3

import os
import glob
from itertools import chain

from PyQt5.QtCore import Qt

# from PyQt5.QtGui import Q
from PyQt5.QtWidgets import (
    QDialog,
    QPushButton,
    QLabel,
    QGridLayout,
    QVBoxLayout,
    QHBoxLayout,
    QLineEdit,
    QSlider,
    QLabel,
    QLineEdit,
    QCheckBox,
    QFrame,
    QProgressBar,
    QButtonGroup,
)

from config import ICON_SIZES, ICON_PATHS
import qss

# from parse_desktop_file import get_icons_themes


class AskWaitProgress(QDialog):
    def __init__(self):

        super(QDialog, self).__init__()

        self.setStyleSheet(qss.PROGRESS_BAR_DIALOG_QSS)

        vbox = QVBoxLayout()
        self.bar = QProgressBar()
        self.bar.setOrientation(Qt.Horizontal)
        self.bar.setMinimum(0)
        self.bar.setMaximum(100)
        self.bar.setValue(0)

        self.label = QLabel()

        vbox.addWidget(self.bar)
        vbox.addWidget(self.label)

        self.setLayout(vbox)

    def update(self, val, text):

        self.bar.setValue(val)
        self.label.setText(text)


class AskSystemIconThemes(QDialog):
    """ Dialog popup to chose an Icon """

    def __init__(self, old_themes):
        super(QDialog, self).__init__()

        self.setStyleSheet(qss.ICON_THEME_DIALOG_QSS)
        vbox = QVBoxLayout(self)

        button_frame = QFrame()
        grid = QGridLayout()
        self.theme_dct = {}
        for i, theme in enumerate(self.get_system_icons_themes()):
            self.theme_dct[theme] = QCheckBox(theme)
            if theme in old_themes:
                self.theme_dct[theme].setChecked(True)
            grid.addWidget(self.theme_dct[theme], i % 8, i // 8)
        button_frame.setLayout(grid)

        vbox.addWidget(button_frame)

        frame = QFrame()
        hbox = QHBoxLayout()

        okBtn = QPushButton("Ok")
        okBtn.clicked.connect(self.accept)

        closeBtn = QPushButton("Cancel")
        closeBtn.clicked.connect(self.close)

        hbox.addWidget(okBtn)
        hbox.addWidget(closeBtn)

        frame.setLayout(hbox)
        vbox.addWidget(frame)

    def get_system_icons_themes(self):

        paths = ICON_PATHS

        themes = set(
            chain(
                *[
                    [
                        f.split("/")[-1]
                        for f in glob.iglob(path + "/*")
                        if os.path.isdir(f)
                    ]
                    for path in paths
                ]
            )
        )

        if "hicolor" in themes:
            themes.remove("hicolor")

        return themes

        # themes_list = ', '.join(themes)

    def get_activated(self):

        activated = {th for th, cb in self.theme_dct.items() if cb.isChecked()}

        #        activated = []
        #        for th, cb in self.theme_dct:
        #            if cb.isChecked():
        #                activated.append(th)
        return activated


class AskIconSize(QDialog):
    """ Ask new icon size"""

    def __init__(self, actual_size):
        super(QDialog, self).__init__()

        self.setStyleSheet(qss.ICON_SIZE_DIALOG_QSS)
        self.layout = QVBoxLayout(self)  # QVBoxLayout(self)

        self.slider = QSlider(Qt.Horizontal)
        self.slider.setMinimum(0)
        self.slider.setMaximum(len(ICON_SIZES) - 1)
        pos = ICON_SIZES.index(actual_size)
        self.slider.setSliderPosition(pos)
        self.slider.setTickInterval(1)
        self.slider.setTickPosition(QSlider.TicksBelow)

        # self.slider.valueChanged.connect()
        self.text = QLabel()
        # self.text.setReadOnly(True)
        self.text.setText(str(actual_size) + " px")
        self.text.setAlignment(Qt.AlignHCenter)

        self.closeBtn = QPushButton("Close")
        self.closeBtn.clicked.connect(self.close)

        self.layout.addWidget(self.slider)
        self.layout.addWidget(self.text)
        self.layout.addWidget(self.closeBtn)

    def get_new_size(self):
        val = self.slider.value()
        text = str(ICON_SIZES[val])
        self.text.setText(text + " px")
        return val


class AskMultipleValues(QDialog):
    """ Dialog to resize icon """

    def __init__(self, *args, **kwargs):

        super(QDialog, self).__init__()
        self.layout = QGridLayout(self)  # QVBoxLayout(self)
        self.textEdits = []

        for i, item in enumerate(args):
            text = QLabel(item)
            textEdit = QLineEdit()
            self.textEdits.append(textEdit)
            self.layout.addWidget(text, i, 0)
            self.layout.addWidget(textEdit, i, 1, 1, 2)

        ok_btn = QPushButton("Ok")
        ok_btn.clicked.connect(self.accept)

        cancel_btn = QPushButton("Cancel")
        cancel_btn.clicked.connect(self.close)

        self.layout.addWidget(cancel_btn, i + 1, 1)
        self.layout.addWidget(ok_btn, i + 1, 2)

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Return:
            self.accept()
        elif e.key() == Qt.Key_Escape:
            self.close()

    def get_values(self):
        vals = []
        for textEdit in self.textEdits:
            vals.append(textEdit.text())
        try:
            if any([int(val) >= 1 for val in vals]):
                return (*vals, True)
            else:
                raise ValueError("Size must be >= 1")
        except ValueError as e:
            print(f"{e}\nwrong value for icon size")
            return 0, 0, False

    def cancel(self):
        return 0, 0, False
